# GESTION DE CENTRO DE COMPUTOS - LOGIN/LOGOUT-API

El trabajo consiste en realizar un deploy con Heroku de una aplicacion cuyos requisitos minimos son los de login/logout, junto con sus respectivos tests.
Se debe utilizar el servicio de CI/CD ofrecido por Gitlab.

## Local
Se tiene una aplicacion que corre de forma local utilizando puertos de forma separada tanto para el servidor-backend:5000 como para el cliente-frontend:3000


### Correr aplicacion de forma local
Para correr de forma local se necesitan descargar sus dependencias, necesarias para su correcto funcionamiento.

Se instalan las dependencias del proyecto:

```
npm install

```
Las dependencias que se instalaran:

```
    "axios": "^0.18.0",
    "bcryptjs": "^2.4.3",
    "body-parser": "^1.19.0",
    "concurrently": "^4.1.0",
    "cors": "^2.8.5",
    "cross-env": "^5.2.0",
    "eslint": "^5.16.0",
    "express": "^4.16.4",
    "express-promise-router": "^3.0.3",
    "fs": "0.0.1-security",
    "joi": "^14.3.1",
    "jsonwebtoken": "^8.5.1",
    "mongodb": "^3.2.4",
    "mongoose": "^5.5.7",
    "morgan": "^1.9.1",
    "nodemon": "^1.19.0",
    "passport": "^0.4.0",
    "passport-jwt": "^4.0.0",
    "passport-local": "^1.0.0",
    "path": "^0.12.7"
```


## Deploy

Estas son las instrucciones que se siguieron para levantar el proyecto en Heroku utilizando Gitlab.

### Prerequisitos

Para poder comenzar se debe contar con:
- Una aplicacion web para deployar
- Una cuenta en Gitlab
- Una cuenta en Heroku


### Antes del Deploy

#### Aplicacion

Se debe tener el proyecto corriendo de forma local, siguiendo los pasos anteriormente mencionados.

#### Heroku
1. Se crea un proyecto en Heroku, sobre el cual se estaria haciendo el deploy de la aplicacion.
2. Se copia el HEROKU_API_KEY para incorporarlo al GitLab.

#### GitLab
1. Se añade el archivo .gitlab-ci.yml con las configuraciones de los distintos stages.
2. Se incorpora el HEROKU_API_KEY al sector de variables del proyecto.
3. Se hbailitan los runners para el proyecto en sus configuraciones.

### Para el deploy

```
git add .
git commit -m "mensaje"
git push origin master
```

### Luego del Deploy

#### Se ejecutan los jobs para hacer el deploy en Heroku
Una vez que se realizo el push al repositorio, Gitlab ejecuta jobs para realizar el deploy en Heroku, realizando una integracion

#### Si el job tiene exito:
Luego de haberse finalizado el job, se genera el deploy correspondiente en Heroku, con el cual se puede revisar la aplicacion y realizar las operaciones correspondientes al signin/signup.


## Tests

Estos tests ayudan a comprobar el funcionamiento de las funcionalidades basicas de la aplicacion web para la parte de sigin/signup.

### Dependencias

Los tests se pueden correr de forma local utilizando distintas herramientas destinadas a esta funcion especifica

```
   "chai": "^4.2.0",
    "chai-http": "^4.3.0",
    "faker": "^4.1.0",
    "mocha": "^6.1.4",
    "nyc": "^14.1.1",
    "rewire": "^4.0.1",
    "sinon": "^7.3.2",
    "sinon-chai": "^3.3.0"
```

### Correr los tests

Se puede correr de la siguiente forma:

```
npm test
```

Correran los tests y daran este resultado:

```

cross-env NODE_ENV=test mocha --timeout 20000 __tests__/server/**/*.test.js

-- Connected to test db --


  > CONTROLLERS
    signUp
      ✓ returns 403 if the user already exists
      ✓ returns 200 if the user was created successfully
      ✓ returns signup fake token
    signIn
Successfull login!
      ✓ returns 200 if tokens returns correctly
Successfull login!
      ✓ returns signin fake token
    dashboard
Got here!!
      ✓ returns resource correctly

  > ROUTES
    signup
      ✓ returns 200 if user was created correctly (1064ms)
      ✓ returns 403 if the email already exists (409ms)
    signin
      ✓ returns 400 if email and password fields are empty
Successfull login!
      ✓ returns 200 if user logged correctly (504ms)
    dashboard
      ✓ returns 401 if user did not login to see resource
Got here!!
      ✓ returns 200 if user logged to see resource (405ms)

 -- Dropped the db from the test --


  12 passing (9s)
```

#### Observaciones sobre los tests
Estos pueden correr exclusivamente una vez ya que se crea un usuario en la base de datos para poder realizar las pruebas respectivas. La segunda vez que se corran los mismos, desplegaran un resultado de que el usuario ya existe ya que la prueba ya se realizo anteriormente.
En cualquier caso, se puede proceder a borrar este usuario que se creo para este unico fin y volver a repetir las pruebas.


## Built con

* [node js](https://nodejs.org/) - lado servidor
* [React](https://reactjs.org/) - lado cliente
* [MongoDB](https://www.mongodb.com/) - base de datos

## Versionamiento

Se utilizo [GitLab](https://gitlab.com/) para el versionamiento.

## Authors

* **Laura Benitez** - *Trabajo Practico* - [lambetzc](https://gitlab.com/lambetzc)
