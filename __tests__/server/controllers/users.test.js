const chai = require('chai');
const faker = require('faker');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const rewire = require('rewire');
const { expect } = chai;

const User = require('../../../server/models/user');
//const User = require('../../../../server/models/user');
const userController = rewire('../../../server/controllers/users.js');
//const userController = rewire('../../../../server/controllers/users.js');
chai.use(sinonChai);

let sandbox = null;

describe('> CONTROLLERS', () => {
  let req = {
    user: {
      id: faker.random.number(),
    },
    value: {
      body: {
        email: faker.internet.email(),
        password: faker.internet.password(),
      },
    },
  };
  let res = {
    json: function() {
      return this;
    },
    status: function() {
      return this;
    },
  };

  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  

  describe('signUp', () => {
    it('returns 403 if the user already exists', async () => {
      sandbox.spy(res, 'json');
      sandbox.spy(res, 'status');
      sandbox.stub(User, 'findOne').returns(
        Promise.resolve({
          id: faker.random.number(),
        }),
      );

      try {
        await userController.signUp(req, res);

        expect(res.status).to.have.been.calledWith(403);
        expect(res.json).to.have.been.calledWith({
          error: 'Email already exists!',
        });
      } catch (error) {
        throw new Error(error);
      }
    });

    it('returns 200 if the user was created successfully', async () => {
      sandbox.spy(res, 'json');
      sandbox.spy(res, 'status');
      sandbox.stub(User, 'findOne').returns(Promise.resolve(false));
      sandbox.stub(User.prototype, 'save').returns(
        Promise.resolve({
          id: faker.random.number(),
        }),
      );

      try {
        await userController.signUp(req, res);

        expect(res.status).to.have.been.calledWith(200);
        expect(res.json.callCount).to.equal(1);
      } catch (error) {
        throw new Error(error);
      }
    });


    it('returns signup fake token', async () => {
      sandbox.spy(res, 'json');
      sandbox.spy(res, 'status');
      sandbox.stub(User, 'findOne').returns(Promise.resolve(false));
      sandbox.stub(User.prototype, 'save').returns(
        Promise.resolve({
          id: faker.random.number(),
        }),
      );

      let signToken = userController.__set__('signToken', user => 'fakeTokenSignUp');

      try {
        await userController.signUp(req, res);

        expect(res.json).to.have.been.calledWith({
          token: 'fakeTokenSignUp',
        });
        signToken();
      } catch (error) {
        throw new Error(error);
      }
    });
  });


  describe('signIn', () => {
    it('returns 200 if tokens returns correctly', async () => {
      sandbox.spy(res, 'json');
      sandbox.spy(res, 'status');

      try {
        await userController.signIn(req, res);

        expect(res.status).to.have.been.calledWith(200);
        expect(res.json.callCount).to.equal(1);
      } catch (error) {
        throw new Error(error);
      }
    });

    it('returns signin fake token', async () => {
      sandbox.spy(res, 'json');
      sandbox.spy(res, 'status');

      // fake jwt token with rewire
      let signToken = userController.__set__('signToken', user => 'fakeTokenSignIn');

      try {
        await userController.signIn(req, res);

        expect(res.json).to.have.been.calledWith({
          token: 'fakeTokenSignIn',
        });
        signToken();
      } catch (error) {
        throw new Error(error);
      }
    });
  });

  describe('dashboard', () => {
    it('returns resource correctly', async () => {
      sandbox.spy(console, 'log');
      sandbox.spy(res, 'json');

      try {
        await userController.dashboard(req, res);

        expect(console.log).to.have.been.called;
        expect(
          res.json.calledWith({
            secret: 'resource',
          }),
        ).to.be.ok;
        expect(res.json).to.have.been.calledWith({
          secret: 'resource',
        });
      } catch (error) {
        throw new Error(error);
      }
    });
  });

});
