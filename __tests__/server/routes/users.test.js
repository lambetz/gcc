const chai = require('chai');
const chaiHttp = require('chai-http');
const faker = require('faker');
const mongoose = require('mongoose');
const {expect} = chai;

//const server = require('../../../../server/app');
const server = require('../../../server/app');

chai.use(chaiHttp);

let token;

describe('> ROUTES', () => {
    const signup = '/users/signup';
    const signin = '/users/signin';
    const dashboard = '/users/dashboard';
    const user = {
      email: faker.internet.email(),
      password: faker.internet.password(),
    };
    const preSave = {
      email: 'pruebatest@pruebatest.com',
      password: faker.internet.password(),
    };
  
    before(async () => {
      const result = await chai
        .request(server)
        .post(signup)
        .send(preSave);
      expect(result.status).to.equal(200);
      token = result.body.token;
    });
  
    // after all test have run we drop our test database
    after('dropping the db from test', async () => {
      await mongoose.connection.dropDatabase(() => {
        console.log('\n -- Dropped the db from the test --');
      });
      await mongoose.connection.close();
    });
  
    describe('signup', () => {
      it('returns 200 if user was created correctly', async () => {
        try {
          const result = await chai
            .request(server)
            .post(signup)
            .send(user);
          expect(result.status).to.equal(200);
          expect(result.body).not.to.be.empty;
          expect(result.body).to.have.property('token');
        } catch (error) {
          console.log(error);
        }
      });
  
      it('returns 403 if the email already exists', async () => {
        try {
          await chai
            .request(server)
            .post(signup)
            .send(preSave);
        } catch (error) {
          expect(error.status).to.equal(403);
          expect(error.response.text).to.equal('{"error":"Email already exists!"}');
        }
      });
    });
  
    describe('signin', () => {
      it('returns 400 if email and password fields are empty', async () => {
        let user = {};
        try {
          const result = await chai
            .request(server)
            .post(signin)
            .send(user);
        } catch (error) {
          expect(error.status).to.be.equal(400);
        }
      });
  
      it('returns 200 if user logged correctly', async () => {
        try {
          const result = await chai
            .request(server)
            .post(signin)
            .send(preSave);
  
          expect(result.status).to.be.equal(200);
          expect(result.body).not.to.be.empty;
          expect(result.body).to.have.property('token');
        } catch (error) {
          throw new Error(error);
        }
      });
    });

  
    describe('dashboard', () => {
      it('returns 401 if user did not login to see resource', async () => {
        try {
          await chai.request(server).get(dashboard);
        } catch (error) {
          expect(error.status).to.equal(401);
          expect(error.response.text).to.equal('Unauthorized');
        }
      });
  
      it('returns 200 if user logged to see resource', async () => {
        try {
          const result = await chai
            .request(server)
            .get(dashboard)
            .set('Authorization', token);
  
          expect(result.status).to.equal(200);
          expect(result.body).to.deep.equal({ secret: 'resource' });
        } catch (error) {
          throw new Error(error);
        }
      });
    });

  });