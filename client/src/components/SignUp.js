import React, { Component } from 'react';
// import { compileFunction } from 'vm';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';


import * as actions from '../actions';
import CustomInput from './CustomInput';

class SignUp extends Component{

    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    async onSubmit(formData){
        console.log('onSubmit got called');
        console.log('formData', formData);
        await this.props.signUp(formData);
        if(!this.props.errorMessage){
            this.props.history.push('/dashboard');
        }
    }

    render(){
        const { handleSubmit } = this.props;
        return (
            <div className="row mt-5">
                <div className="col-md-6 m-auto">
                    <div className="card card-body">
                        <h1 className="text-center mb-3"><i class="fas fa-sign-in-alt"></i>  Sign Up</h1>
                        
                        <form onSubmit={handleSubmit(this.onSubmit)}>
                        <div className="form-group">
                            <fieldset>
                                <Field
                                    name= "email"
                                    type="text"
                                    id="email"
                                    label="Enter your email"
                                    placeholder="example@example.com"
                                    component={CustomInput} />
                            </fieldset>
                            </div>
                            <div className="form-group">
                            <fieldset>
                                <Field
                                    name="password"
                                    type="password"
                                    label="Enter your password"
                                    placeholder="password"
                                    id="password"
                                    component={CustomInput} />
                                </fieldset>
                                </div>
                                {this.props.errorMessage ?
                                <div className="alert alert-danger">
                                    {this.props.errorMessage}
                                </div> : null }


                                <button type="submit" className="btn btn-primary">Sign Up</button>
                        </form>
                    </div>
                </div>
   
            </div>
        );
    }
}

function mapStateToProps(state){
    return{
        errorMessage: state.auth.errorMessage
    }
}

export default compose(
    connect(mapStateToProps, actions),
    reduxForm({form:'signup'})
)(SignUp)