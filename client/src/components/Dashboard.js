import React, { Component } from 'react';
// import { getSecret } from '../actions';
import { connect } from 'react-redux';
import * as actions from '../actions';


class Dashboard extends Component{
    async componentDidMount(){
        this.props.getSecret();
    }

    render(){
        return (
            <div>
                
                <br/>
                The resource is: <h2>{this.props.secret}</h2>
            </div>
        );
    }
}

function mapStateToProps(state){
    return{
        secret: state.dash.secret
    }
}

export default connect(mapStateToProps,actions)(Dashboard);