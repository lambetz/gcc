import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../actions';
// import { timingSafeEqual } from 'crypto';


class Header extends Component {

    constructor(props){
        super(props);
        this.signOut = this.signOut.bind(this);
    }

    signOut(){
        console.log('signing out');
        this.props.signOut();
    }


    render(){
        return (



            
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <Link className="navbar-brand" to="/">GCC</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link className="nav-link" to="/dashboard"> Dashboard<span class="sr-only">(current)</span></Link>
                        </li>
                    </ul>
                    <ul className="nav navbar-nav ml-auto">
                    {/* {!this.props.isAuth ?
                        [<li className="nav-item" key="signup">
                            <Link className="nav-link" to="/signup">Sign Up</Link>
                        </li>,
                        <li className="nav-item" key="signin">
                            <Link className="nav-link" to="/signin">Sign In</Link>
                        </li>] : null } */}
                    {this.props.isAuth ?
                        <li className="nav-item">
                            <Link className="nav-link" to="/" onClick={this.signOut}>Sign Out</Link>
                        </li> : null }

                    </ul>
                </div>
            </nav>
        )
    }
}

function mapStateToProps(state){
    return{
        isAuth: state.auth.isAuthenticated
    };
}

export default connect(mapStateToProps, actions)(Header);