import React from 'react';

export default() =>{
    return (
        <div className="row mt-5">
            <div className="col-md-6 m-auto">
            <div className="card card-body text-center">
                <h1><i className="fa fa-user fa-3x"></i></h1>
                <p>Create an account or Sign In if you have one</p>
                <a href="/signup" className="btn btn-primary btn-block mb-2">Sign Up</a>
                <a href="/signin" className="btn btn-secondary btn-block">Sign In</a>
            </div>
            </div>
        </div>
    );
};