import axios from 'axios';
import { AUTH_SIGN_UP } from './types';
import { AUTH_ERROR } from './types';
import { AUTH_SIGN_OUT } from './types';
import { AUTH_SIGN_IN } from './types';
import { DASHBOARD_GET_DATA } from './types';

export const signUp = data => {
    return async dispatch => {
        try{


            if (process.env.NODE_ENV === 'production') {
                const res = await axios.post('https://login-logout-api.herokuapp.com/users/signup', data);
                console.log('res', res);
                dispatch({
                type: AUTH_SIGN_UP,
                payload: res.data
            });
                localStorage.setItem('JWT_TOKEN', res.data.token);
                axios.defaults.headers.common['Authorization'] = '';
            } else {
                const res  = await axios.post('http://localhost:5000/users/signup', data);
                dispatch({
                type: AUTH_SIGN_UP,
                payload: res.data.token
            });
                localStorage.setItem('JWT_TOKEN', res.data.token);
                axios.defaults.headers.common['Authorization'] = res.data.token;
            }




            
            // // const res = await axios.post('https://login-logout-api.herokuapp.com/signup', data);
            // const res = await axios.post('http://localhost:5000/users/signup',data)
            // console.log('res', res);
            // dispatch({
            //     type: AUTH_SIGN_UP,
            //     payload: res.data.token
            // });
            // localStorage.setItem('JWT_TOKEN', res.data.token);
            // axios.defaults.headers.common['Authorization'] = res.data.token;
        }catch(err){
            dispatch({
                type: AUTH_ERROR,
                payload: 'Email already exists'
            })

            console.log('err', err);
        }

    };
}

export const signIn = data => {
    return async dispatch => {
        try{




            if (process.env.NODE_ENV === 'production') {
                const res = await axios.post('https://login-logout-api.herokuapp.com/users/signin', data);
                console.log('res', res);

                dispatch({
                    type: AUTH_SIGN_IN,
                    payload: res.data.token
                });
    
                localStorage.setItem('JWT_TOKEN', res.data.token);
                axios.defaults.headers.common['Authorization'] = res.data.token;
            } else {
                const res = await axios.post('http://localhost:5000/users/signin', data);
    
                dispatch({
                    type: AUTH_SIGN_IN,
                    payload: res.data
                });
       
            
                localStorage.setItem('JWT_TOKEN', res.data.token);
                axios.defaults.headers.common['Authorization'] = '';
    
            }


            
            // // const res = await axios.post('https://login-logout-api.herokuapp.com/signin', data);
            // const res = await axios.post('http://localhost:5000/users/signin',data)
            // console.log('res', res);
            // dispatch({
            //     type: AUTH_SIGN_IN,
            //     payload: res.data.token
            // });
            // localStorage.setItem('JWT_TOKEN', res.data.token);
            // axios.defaults.headers.common['Authorization'] = res.data.token;
        }catch(err){
            dispatch({
                type: AUTH_ERROR,
                payload: 'User and password does not match.'
            })

            console.log('err', err);
        }

    };
}

export const signOut = () => {
    return dispatch => {
        localStorage.removeItem('JWT_TOKEN');
        axios.defaults.headers.common['Authorization'] = '';

        dispatch({
            type: AUTH_SIGN_OUT,
            payload: ''
        })

    };
}

export const getSecret = () => {
    return async dispatch => {
        try{
            console.log('Getting secret')


            if (process.env.NODE_ENV === 'production') {
                const res = await axios.get('https://login-logout-api.herokuapp.com/users/dashboard');
                console.log('res', res);

                dispatch({
                    type: DASHBOARD_GET_DATA,
                    payload: res.data.secret
                })
            } else {

                const res = await axios.get('http://localhost:5000/users/dashboard');
                console.log('res', res);

                dispatch({
                    type: DASHBOARD_GET_DATA,
                    payload: res.data.secret
               })
            }



            // // const res = await axios.get('https://login-logout-api.herokuapp.com/users/dashboard')
            // const res = await axios.get('http://localhost:5000/users/dashboard')
            // dispatch({
            //     type: DASHBOARD_GET_DATA,
            //     payload: res.data.secret
            // })
        
        }catch(err){
            console.error('err', err);
        }

    }
}