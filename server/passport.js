const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const { JWT_SECRET } = require('./configuration');

const User = require('./models/user');

//JSON WEB TOKENS STRATEGY
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: JWT_SECRET
}, async(payload, done) => {
    try{
        //find user specified in token
        const user = await User.findById(payload.sub);
        
        
        //handle if user does not exist
        if(!user){
            return done(null, false);
        }

        //return the user otherwise
        done(null, user);
        
    }catch(error){
        done(error, false);
    }
}));

//LOCAL STRATEGY
passport.use(new LocalStrategy({
    usernameField: 'email'
}, async (email, password, done) => {
    try{
        //given the email, find the user
        const user = await User.findOne({email});

        //handle if user is not found
        if(!user){
            return done(null, false);
        }

        //check if password matches
        const isMatch = await user.isValidPassword(password);

        //handle if it does not match
        if(!isMatch){
            return done(null, false);
        }

        //return the user otherwise
        done(null, user);
    }catch(error){
        done(error, false);
    }

}));