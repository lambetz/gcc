const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.Promise = global.Promise;
if(process.env.NODE_ENV == 'test'){
    console.log("-- Connected to test db --");
    // mongoose.connect('mongodb://localhost/loginlogoutTEST',{
    //     useCreateIndex:true,
    //     useNewUrlParser:true
    // });
   mongoose.connect('mongodb+srv://admin:admin@mongodb-oo7wm.azure.mongodb.net/loginlogoutTEST?retryWrites=true',{
    useCreateIndex:true,
    useNewUrlParser:true
   });

}else{
    console.log("-- Connected to dev db --");
    // mongoose.connect('mongodb://localhost/loginlogout',{
    //     useCreateIndex:true,
    //     useNewUrlParser:true
    // });
    mongoose.connect('mongodb+srv://admin:admin@mongodb-oo7wm.azure.mongodb.net/loginlogout?retryWrites=true',{
        useCreateIndex:true,
        useNewUrlParser:true
    });
}



const app = express();

app.use(cors());

//Middleware
if(!process.env.NODE_ENV == 'test'){
    app.use(morgan('dev'));
}


// const path = require('path')
// if (process.env.NODE_ENV === 'production') {
//     app.use(express.static('client/build'));
//     // const path = require('path');
//     app.get('*', (request, response) => {
// 	response.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
// });
// }





app.use(bodyParser.json());

//Routes
app.use('/users', require('./routes/users'));

//Server Start
// const port = process.env.PORT || 3000;
// app.listen(port);
// console.log(`Listening on Port ${port} `);

//ESTE FUNCIONA ABAJO Y NO ARRIBA
// if (process.env.NODE_ENV === 'production') {
//     // app.use(express.static(path.join(__dirname, 'client/build')));
//     app.use(express.static(__dirname+'../client/build'));

//     // Handle React routing, return all requests to React app
//     const path = require('path'); 
//     app.get('*', function(req, res) {
//       res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
//     });
// }

//ESTE FUNCIONA ABAJO Y NO ARRIBA
//  if (process.env.NODE_ENV === 'production') {

//     // const path = require('path');
//     // app.use(express.static(path.join(__dirname + 'client/build')));
//     app.use(express.static(__dirname + 'client/build'));

//     const path = require('path'); 
//     app.get('*', function(req, res) {
//       res.sendFile(path.resolve(__dirname, '/client/build', 'index.html'));
//     });

//     console.log(path.join(__dirname, '/client/build', 'index.html'));

//  }

//ESTE FUNCIONA ABAJO Y ARRIBA EN EL FRONT, EL BACK EN NINGUNO
// if (process.env.NODE_ENV === 'production') {
//     // Serve any static files
// //    var path = require('path');
//     app.use(express.static(path.join(__dirname, 'client/build')));
//   // Handle React routing, return all requests to React app
//     var path = require('path'); 
//     app.get('*', function(req, res) {
//       res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
//     });
// }

// //ESTE FUNCIONA ABAJO: FRONT Y BACK y ARRIBA: BACK
// if (process.env.NODE_ENV === 'production') {
//     app.use(express.static('client/build'));
  
//     const path = require('path');
//     app.get('*', (req, res) => {
//       res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
//     });
//   }
  


// //STE FUNCIONA ABAJO Y ARRIBA EN EL FRONT(paginapricipal), EL BACK EN AMBOS
// if (process.env.NODE_ENV === 'production') {
//     // Serve any static files
// //    var path = require('path');
//     app.use(express.static('client/build'));
//   // Handle React routing, return all requests to React app
//     var path = require('path'); 
//     app.get('*', function(req, res) {
//       res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
//     });
// }


// ABAJO: FRONT Y BACK, ARRIBA:
if (process.env.NODE_ENV === 'production') {
    // Serve any static files
//    var path = require('path');
    app.use(express.static('client/build'));
  // Handle React routing, return all requests to React app
    var path = require('path'); 
    app.get('*', function(req, res) {
      res.sendFile(path.resolve('client/build', 'index.html'));
    });
}


// if (process.env.NODE_ENV === 'production') {
//     // Exprees will serve up production assets
//     app.use(express.static('../client/build'));
  
//     // Express serve up index.html file if it doesn't recognize route
//     const path = require('path');
//     app.get('*', (req, res) => {
//       res.sendFile(path.resolve(__dirname, '../client/build/index.html'));
//       console.log(path.resolve(__dirname, '../client/build/index.html'))
//     });
// }



module.exports = app;