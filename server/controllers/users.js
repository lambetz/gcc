const JWT = require('jsonwebtoken');

const User = require('../models/user');

const {JWT_SECRET} = require('../configuration');

signToken = user => {

    return JWT.sign({
        iss: 'Something',
        sub: user.id,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate()+1)
    }, JWT_SECRET);

}

module.exports = {
    signUp: async(req,res,next) =>{
        //email and pass
        // console.log('req.value.body', req.value.body);
        // console.log('UsersController.signUp() has been called.');

        // const {email, password} = req.value.body;
        const email = req.value.body.email;
        const password = req.value.body.password;
        
        //check if email already exists
        const foundUser = await User.findOne({
            email: email
        });

        if(foundUser){
            return res.status(403).json({error: "Email already exists!"});
        }
        
        
        const newUser = new User({
            email: email,
            password: password
        });

        await newUser.save();

        // res.json({ user: 'created'});

        const token = signToken(newUser);

        res.status(200).json({token: token});
    },

    signIn: async(req,res,next) =>{
        //generate token
        // console.log('UsersController.signIn() has been called.');
        //console.log('req.user', req.user());
        const token = signToken(req.user);
        res.status(200).json({token});

        console.log("Successfull login!");
    },

    dashboard: async(req,res,next) =>{
        // console.log('UsersController.signUp() has been called.');
        console.log('Got here!!');
        
        res.json({
            secret: 'resource'
        });
        


    }
}

