const app = require('./app');

//starting the server
const port = process.env.PORT || 5000;

app.listen(port);

console.log(`Listening at port ${port}`);